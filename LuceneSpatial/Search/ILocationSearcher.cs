﻿using System.Collections.Generic;
using LuceneSpatial.Search.Models;

namespace LuceneSpatial.Search
{
    public interface ILocationSearcher
    {
        IEnumerable<LocationSearchResult> Search(double lat, double lng);
    }
}
