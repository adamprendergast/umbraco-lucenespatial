﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using Examine;
using Examine.LuceneEngine.Providers;
using Lucene.Net.Documents;
using Lucene.Net.Search;
using Lucene.Net.Search.Function;
using Lucene.Net.Spatial.Queries;
using Lucene.Net.Spatial.Vector;
using Lucene.Net.Store;
using LuceneSpatial.Search.Models;
using Spatial4n.Core.Context;
using Spatial4n.Core.Distance;
using Spatial4n.Core.Shapes;

namespace LuceneSpatial.Search
{
    public class LocationSearcher : ILocationSearcher
    {
        private readonly IExamineManager _examineManager;
        //private readonly IndexSearcher _searcher;

        public LocationSearcher(IExamineManager examineManager)
        {
            _examineManager = examineManager;
            //var pathToIndex = "~/App_Data/TEMP/ExamineIndexes/LocationIndex";
            //var indexDirectory = FSDirectory.Open(new DirectoryInfo(HttpContext.Current.Server.MapPath(pathToIndex)));
            //_searcher = new IndexSearcher(indexDirectory, readOnly: true);
        }

        public IEnumerable<LocationSearchResult> Search(double lat, double lng)
        {
            if (!_examineManager.TryGetIndex("LocationIndex", out var externalIndex))
            {
                throw new InvalidOperationException("No LuceneIndex found with name " + Umbraco.Core.Constants.UmbracoIndexes.ExternalIndexName);
            }

            var examineSearcher = externalIndex.GetSearcher() as LuceneSearcher;
            var _searcher = examineSearcher.GetLuceneSearcher();

            var results = new List<LocationSearchResult>();

            var searchRadius = 300; // in KM

            SpatialContext ctx = SpatialContext.GEO;

            //PointVectorStrategy strategy = new PointVectorStrategy(ctx, "geoLocation");

            var strategy = GeoCoordSearching.GetRecursivePrefixTreeStrategy(ctx, "geoLocation");

            GeoCoordSearching.GetXYFromCoords(lat, lng, out var x, out var y);

            // Make a circle around the search point
            var args = new SpatialArgs(
                SpatialOperation.Intersects,
                ctx.MakeCircle(x, y, DistanceUtils.Dist2Degrees(searchRadius, DistanceUtils.EARTH_MEAN_RADIUS_KM)));

            // I'm 99% certain the above 'args' highlights the error. If you pass in the maximum 'distance' value to .MakeCircle()
            // ... this happens to be '180' as it expects degrees (I don't fully understand this)
            // ... then it brings the missing results back, but they are not in the correct sort order. They are at the end of the list.

            // Comment out below to try with max circle
            //var args = new SpatialArgs(
            //    SpatialOperation.Intersects,
            //    ctx.MakeCircle(lat, lng, 180));


            Filter filter = strategy.MakeFilter(args);

            //var field = new SortField("score", SortField.SCORE, true);
            //var sort = new Sort(field);

            //Query q = strategy.MakeQueryDistanceScore(args);

            TopDocs topDocs = _searcher.Search(new MatchAllDocsQuery(), filter, _searcher.MaxDoc, new Sort(new SortField(null, SortField.SCORE)));

            ScoreDoc[] scoreDocs = topDocs.ScoreDocs;

            foreach (ScoreDoc s in scoreDocs)
            {
                Document doc = _searcher.Doc(s.Doc);

                results.Add(new LocationSearchResult
                {
                    Name = doc.GetField("nodeName").StringValue,
                });
            }

            return results;
        }
    }
}