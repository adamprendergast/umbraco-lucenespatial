﻿using Examine;
using Examine.LuceneEngine;
using Examine.LuceneEngine.Providers;
using Lucene.Net.Documents;
using Lucene.Net.Spatial.Vector;
using Spatial4n.Core.Context;
using Spatial4n.Core.Shapes;
using Umbraco.Core.Composing;
using Umbraco.Web;
using Umbraco.Web.PublishedModels;

namespace LuceneSpatial.Search
{
    public class LocationSearchComponent : IComponent
    {
        private readonly IExamineManager _examineManager;
        private readonly IUmbracoContextFactory _umbracoContextFactory;
        private readonly LocationSearchIndexCreator _locationSearchIndexCreator;

        //private SpatialContext _spatialContext;
        //private PointVectorStrategy _strategy;

        public LocationSearchComponent(IExamineManager examineManager,
            IUmbracoContextFactory umbracoContextFactory,
            LocationSearchIndexCreator locationSearchIndexCreator)
        {
            _examineManager = examineManager;
            _umbracoContextFactory = umbracoContextFactory;
            _locationSearchIndexCreator = locationSearchIndexCreator;
        }

        public void Initialize()
        {
            foreach (var index in _locationSearchIndexCreator.Create())
            {
                _examineManager.AddIndex(index);

                if (index is LuceneIndex luceneIndex)
                {
                    luceneIndex.DocumentWriting += LuceneIndex_DocumentWriting;
                }
            }
        }

        private void LuceneIndex_DocumentWriting(object sender, DocumentWritingEventArgs e)
        {
            using (var contextRef = _umbracoContextFactory.EnsureUmbracoContext())
            {
                var contentCache = contextRef.UmbracoContext.Content;

                if (!int.TryParse(e.ValueSet.Id, out var id))
                {
                    return;
                }

                var location = (Location)contentCache.GetById(id);

                double lat = (double)location.Latitude;
                double lng = (double) location.Longitude;

                // Start of Lucene.Net.Contrib.Spatial usage
                SpatialContext ctx = SpatialContext.GEO;
                var strategy = GeoCoordSearching.GetRecursivePrefixTreeStrategy(ctx, "geoLocation");

                //PointVectorStrategy strategy = new PointVectorStrategy(ctx, "geoLocation");
                GeoCoordSearching.GetXYFromCoords(lat, lng, out var x, out var y);

                Shape geoPoint = ctx.MakePoint(x, y);

                foreach (AbstractField field in strategy.CreateIndexableFields(geoPoint))
                {
                    e.Document.Add(field);
                }
            }
        }

        public void Terminate()
        {
        }
    }
}