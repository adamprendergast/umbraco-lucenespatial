﻿using Lucene.Net.Spatial.Prefix;
using Lucene.Net.Spatial.Prefix.Tree;
using Spatial4n.Core.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LuceneSpatial.Search
{
    public static class GeoCoordSearching
    {
        public static RecursivePrefixTreeStrategy GetRecursivePrefixTreeStrategy(SpatialContext ctx, string fieldName)
        {
            int maxLevels = 11; //results in sub-meter precision for geohash
            SpatialPrefixTree grid = new GeohashPrefixTree(ctx, maxLevels);
            RecursivePrefixTreeStrategy strategy2 = new RecursivePrefixTreeStrategy(grid, fieldName);
            return strategy2;
        }

        public static void GetXYFromCoords(double lat, double lng, out double x, out double y)
        {
            // Important! we need to change to x/y coords, longitude = x, latitude = y
            x = lng;
            y = lat;
        }
    }
}