﻿namespace LuceneSpatial.Search.Models
{
    public class LocationSearchResult
    {
        public string Name { get; set; }
    }
}