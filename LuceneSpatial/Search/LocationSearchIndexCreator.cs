﻿using System.Collections.Generic;
using Examine;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Util;
using Umbraco.Core.Logging;
using Umbraco.Core.Services;
using Umbraco.Examine;
using Umbraco.Web.Search;

namespace LuceneSpatial.Search
{
    public class LocationSearchIndexCreator : LuceneIndexCreator, IUmbracoIndexesCreator
    {
        private readonly IProfilingLogger _profilingLogger;
        private readonly ILocalizationService _localizationService;
        private readonly IPublicAccessService _publicAccessService;

        private const string INDEX_NAME = "LocationIndex";

        public LocationSearchIndexCreator(IProfilingLogger profilingLogger, ILocalizationService localizationService, IPublicAccessService publicAccessService)
        {
            _profilingLogger = profilingLogger;
            _localizationService = localizationService;
            _publicAccessService = publicAccessService;
        }

        public override IEnumerable<IIndex> Create()
        {
            var directory = CreateFileSystemLuceneDirectory(INDEX_NAME);
            var fieldDefinitions = new UmbracoFieldDefinitionCollection();
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            var valueSetValidator = new ContentValueSetValidator(
                publishedValuesOnly: true,
                supportProtectedContent: false,
                publicAccessService: _publicAccessService,
                includeItemTypes: new[] { "location" });

            var index = new UmbracoContentIndex(INDEX_NAME,
                directory,
                fieldDefinitions,
                analyzer,
                _profilingLogger,
                _localizationService,
                valueSetValidator);

            return new[] { index };
        }
    }
}