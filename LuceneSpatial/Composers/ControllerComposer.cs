﻿using LuceneSpatial.Controllers.API;
using Umbraco.Core;
using Umbraco.Core.Composing;

namespace LuceneSpatial.Composers
{
    [RuntimeLevel(MinLevel = RuntimeLevel.Run)]
    public class ControllerComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Register<GeoSearchController>();
        }
    }
}