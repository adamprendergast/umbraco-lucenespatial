﻿using LuceneSpatial.Search;
using Umbraco.Core;
using Umbraco.Core.Composing;

namespace LuceneSpatial.Composers
{
    [RuntimeLevel(MinLevel = RuntimeLevel.Run)]
    public class SearchComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Components().Append<LocationSearchComponent>();
            composition.RegisterUnique<LocationSearchIndexCreator>();
            composition.Register<ILocationSearcher, LocationSearcher>();
        }
    }
}