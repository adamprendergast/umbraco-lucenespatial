﻿using System.Collections.Generic;
using System.Web.Http;
using LuceneSpatial.Search;
using LuceneSpatial.Search.Models;
using Umbraco.Web.WebApi;

namespace LuceneSpatial.Controllers.API
{
    public class GeoSearchController : UmbracoApiController
    {
        private readonly ILocationSearcher _locationSearcher;

        public GeoSearchController(ILocationSearcher locationSearcher)
        {
            _locationSearcher = locationSearcher;
        }

        // GET: GeoSearch
        [HttpGet]
        public IEnumerable<LocationSearchResult> Search([FromUri] double lat, [FromUri] double lng)
        {
            return _locationSearcher.Search(lat, lng);
        }
    }
}